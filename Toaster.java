import java.util.Scanner;
public class Toaster{
	private String color;
	private int temperature;//it show the level of the temperature. 1 is the minimum, 5 is the maximum.
	private int volt;
	private String bread;
	private int slice;//it show the slice that one toaster can toast at same time.
	private String mode;
	
	public void toast(){
		System.out.println("Toaster: OK! Now I am start to toasting a "+this.bread+" ,the level of the temperature is "+this.temperature+"and the mode is: "+this.mode);
		System.out.println("Toaster: It's done! Enjoy your toast!");
	}
	public int setLvOfTemperature(int newLvOfTemperature){
		/*variable finish is for the loop in the main method.
		  0 means continue, 1 means finish.
		*/
		int finish=0;
		if(newLvOfTemperature>=1&&newLvOfTemperature<=5){
			this.temperature=newLvOfTemperature;
			finish=1;
		}else{
		System.out.println("Error: the level of the temperature must between 1 and 5, please try again");
		}
		return finish;
	}
	
	//grade lab code
	public void changeMode(int modeNumber){
		modeNumber=verifiedModeNumber(modeNumber);
		if(modeNumber==1){
			this.mode="normal";
		}
		if(modeNumber==2){
			this.mode="bagel";
		}
		
	}
	private int verifiedModeNumber(int modeNumber){
		boolean isNotValid=true;
		int result=0;
		Scanner reader= new Scanner(System.in);
		while(isNotValid==true){
			if(modeNumber>0&&modeNumber<=2){
				result=modeNumber;
				isNotValid=false;
			}else{
				System.out.println("Error: the mode number should between 1-2, please try again");
				modeNumber=reader.nextInt();
			}
		}
		return result;
	}
	public void printToasterInfo(){
		System.out.println("Your toaster infomation:");
		System.out.println("color: "+this.color);
		System.out.println("temperature:"+this.temperature);
		System.out.println("volt: "+this.volt);
		System.out.println("slice(max): "+this.slice);
		System.out.println("Mode:"+this.mode);
	}
	
	public void printToasterInfoV2(){
		System.out.println("Your toaster infomation:");
		System.out.println("color: "+this.color);
		System.out.println("volt: "+this.volt);
		System.out.println("slice(max): "+this.slice);
	}
	
	
	public String getColor(){
		return this.color;
	}
	public void setColor(String newColor){
		this.color=newColor;
	}
	
	public int getTemperature(){
		return this.temperature;
	}
	
	
	public int getVolt(){
		return this.volt;
	}
	public void setVolt(int newVolt){
		if(newVolt>=103&&newVolt<=120){
			this.volt=newVolt;
		}else{
			System.out.println("Please enter a valid number. You should enter a number between 103-120.");
		}
	}
	
	public String getBread(){
		return this.bread;
	}
	public void setBread(String newBread){
		this.bread=newBread;
	}
	
	public int getSlice(){
		return this.slice;
	}
	public void setSlice(int newSlice){
		this.slice=newSlice;
	}
	
	public String getMode(){
		return this.mode;
	}
	public void setMode(String newMode){
		this.mode=newMode;
	}
	
	
	//constructor
	public Toaster(String newColor, int newVolt, int newSlice){
		this.color=newColor;
		this.temperature=1;
		this.volt=newVolt;
		this.slice=newSlice;
		this.mode="normal";
		this.bread="whiteBread";
	}
	
}