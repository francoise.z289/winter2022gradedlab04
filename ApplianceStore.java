import java.util.Scanner;
public class ApplianceStore{
	public static void main(String[]args){
		Toaster[] toasters=new Toaster[4];
		Scanner reader=new Scanner(System.in);
		String tempColor=" ";
		int tempVolt=0;
		int tempSlice=0;
		
		for(int setLoop=0;setLoop<toasters.length;setLoop++){
			System.out.println("Set toaster "+(setLoop+1)+".  First, enter the color of the toaster.");
			tempColor=reader.next();
			System.out.println("Then, tell me how many slice of bread you want toast at the same time.");
			tempSlice=reader.nextInt();
			System.out.println("Finally, set the volt.");
			tempVolt=reader.nextInt();
			
			Toaster temp=new Toaster(tempColor,tempVolt,tempSlice);
			toasters[setLoop]=temp;
		}
		System.out.println("Well,this is your last toaster:  color: "+toasters[toasters.length-1].getColor()+"  slices: "+toasters[toasters.length-1].getSlice()+"  volt:"+toasters[toasters.length-1].getVolt());
		
		
		toasters[3].setColor("Green");		
		toasters[3].setSlice(2);
		toasters[3].setVolt(120);
		
		toasters[3].printToasterInfoV2();
		
		
		System.out.println("Add a bread in your toaster.");
		toasters[0].setBread(reader.next());
		
		
		System.out.println("Now set the level of the temperature. Note that the level must between 1 and 5(include these two number).");
		/*variable finish is for the loop below.
		  0 means continue, 1 means finish.
		*/
		int finish=0;
		while(finish==0){
			finish=toasters[0].setLvOfTemperature(reader.nextInt());
		}
		
		
		toasters[0].toast();
		
		//graded lab 
		System.out.println("Now change the mode of the toaster. Please enter 1 or 2.");
		System.out.println("1 means normal mode, 2 means bagel mode.");
		int tempModeNumber=reader.nextInt();
		toasters[1].changeMode(tempModeNumber);
		toasters[1].printToasterInfo();
		
		
	}
}